Travail pratique 2
==================

Étudiants
---------

- Nicolas Gere
- Cyril Ibrahim

Évaluation
----------

+-------------------------+----------------------------+-----------+-----------+
| Critère                 | Sous-critère               | Note      | Sur       |
+=========================+============================+===========+===========+
|                         | Rues et terrains           | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Immeubles commerciaux      | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Immeubles résidentiels     | 15        | 15        |
| Fonctionnabilité        +----------------------------+-----------+-----------+
|                         | Lieux publics              | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Animation                  | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Détection de collision     | 20        | 20        |
|                         +----------------------------+-----------+-----------+
|                         | Paramétrisation            | 5         | 5         |
+-------------------------+----------------------------+-----------+-----------+
|                         | Rues et lieux publics      | 10        | 10        |
|                         +----------------------------+-----------+-----------+
| Qualité graphique       | Immeubles                  | 10        | 10        |
|                         +----------------------------+-----------+-----------+
|                         | Animation                  | 8         | 10        |
+-------------------------+----------------------------+-----------+-----------+
|                         | Fichier README             | 10        | 10        |
|                         +----------------------------+-----------+-----------+
| Qualité de la remise    | Style de programmation     | 15        | 15        |
|                         +----------------------------+-----------+-----------+
|                         | Utilisation de Git         | 10        | 10        |
+-------------------------+----------------------------+-----------+-----------+
| Total                                                | 148       | 150       |
+-------------------------+----------------------------+-----------+-----------+

Commentaires
------------

- Belle qualité graphique et animations !
- Très beaux modèles.
- Les déplacements ne sont pas toujours fluides. De plus, des fois, en
  tournant, ça bloque parfois sans raison apparente.
- README minimal et ne respectant pas le format Markdown. Étant donné tout le
  travail que vous avez faits, ça ne vous aurait pris que quelques minutes !
- Bonne décomposition modulaire.
- Bonne documentation.
- Bonne utilisation de Git.
