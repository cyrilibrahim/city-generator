//MATRIX CITY GENERATOR


/*Ce script permet la génération de matrice 2d comprenant des elements d'un ville d'un point de vue du haut 
  Voici l'équivalence des elements
	- 'v' = rue vertical		taille 1
	- 'h' = rue horizontal		taille 1
	- 'c' = croisement de rue	taille 1
	- 'p' = espace public	  	taille 4x4
	- 'r' = place residentiel   taille 2x2
	- 'b' = building			taille 3x3
	- 'd' = divers(ex: arbre)	taille 1
*/


function createCityMatrix(config){
	
	//Creation of the matrix
	var cityMatrix = [];
	for(var i=0; i< config.HEIGHT_CITY; i++) {
		cityMatrix[i] = [];
		for(var j=0; j< config.WIDTH_CITY; j++) {
			cityMatrix[i][j] = 0;
		}
	}
	
	//Add horizontal roads
	cityMatrix = addHorizontalRoad(cityMatrix);
	
	//Add vertical roads 
	cityMatrix = addVerticalRoad(cityMatrix);
	
	//Add elements of the city 
	cityMatrix = addCityElements(cityMatrix,config);
	
	return cityMatrix;
}






//Met les routes horizontal 
//Prend en parametre la matrice puis la retourne avec les routes horizontal
//Une route verticale vaut 'h'
function addHorizontalRoad(matrix){
	
	//On prend 1/3 eme de la ville en rue 
	var nbOfRoad = matrix.length /4;
	nbOfRoad = Math.floor(nbOfRoad);

	//On genere des rues aleatoirement à l'horizontal
	for(var i = 0; i < nbOfRoad; i++){
		
		var numRoad = randomBetween(0, matrix.length);
		
		for(var j=0; j< matrix[numRoad].length;j++){
			matrix[numRoad][j] = 'h';
		}	
	}
	return matrix;
}

//Met les routes vertical
//Prend en parametre la matrice puis la retourne avec les routes vertical
//Une route verticale vaut 'v'
//Si il y a un croisement on genere un croisement 'c' 
function addVerticalRoad(matrix){
	
	//On prend 1/3 eme de la ville en rue 
	var nbOfRoad = matrix[0].length /4;
	nbOfRoad = Math.floor(nbOfRoad);

	//On genere des rues aleatoirement à la vertical
	for(var i = 0; i < nbOfRoad; i++){
		
		var numRoad = randomBetween(0, matrix[0].length);
		
		for(var j=0; j< matrix.length;j++){
			
			if(matrix[j][numRoad] != 'h' && matrix[j][numRoad] != 'c'){
				matrix[j][numRoad] = 'v';
			}else{
				matrix[j][numRoad] = 'c';
			}
		}	
	}
	return matrix;
}






//Rajoute les elements 'b' (building) 'r' residentiel 'p' public et 'd' divers dans les cases vides 
function addCityElements(matrix,config){
	
	//Parcours de chaque case
	for(var i=0; i<matrix.length; i++) {
		for(var j=0; j<matrix[i].length; j++) {
			//Une case est vide
			if(matrix[i][j] == 0){
				
				//Flag permettant de savoir quand un batiment est ajouter 
				var elementAdded = false;
				var tryCounter = 10;
				
				while(!elementAdded && tryCounter > 0){
					
					var cityElement = randomCityElementWithProbability(config.BUILDING_PRESENCE,config.RESIDENTIAL_PRESENCE,config.PUBLIC_PLACE_PRESENCE);				
					var sizeElement = getSizeElement(cityElement);

					
					if (IsPlaceAvaible(matrix,sizeElement,i,j)){
						
						matrix = addElementToPlace(matrix,sizeElement,i,j,cityElement);
						elementAdded = true;
					}
	
					 tryCounter --;
				}
				
				//Si aucun element ne rentre au bout de 10 essai on met un element 'd' pour décor qui vaut 1
				if((!elementAdded) && (tryCounter <= 0)){
					matrix[i][j] = 'd';
				}
			}	
		}
	}
	
	return matrix;
}

//FONCTION AUXILIAIRE 


//Affiche une matrice 2d en javascript
function showMatrix(matrix){
	
	for(var i=0; i<matrix.length; i++) {
		var toPrint = "[";
		for(var j=0; j<matrix[i].length; j++) {
			toPrint = toPrint +  " " + matrix[i][j] + " ";
		}
		toPrint = toPrint + " ] " + i;
		
		console.log(toPrint);
	}
	
}

//Add element 
function addElementToPlace(matrix, size, x, y,element){
	
	//on parcours la sous-matrice pour ajouter l'element a sa place

	for(var i=0; i<size; i++) {
		for(var j=0; j < size; j++) {
			if(matrix[i+x][j+y] == 0){
				matrix[i+x][j+y] = element;
			}
		}
	}
	
	return matrix; 
}

//Add element 
function addElementToMap(matrix, size, x, y,element){
	
	//on parcours la sous-matrice pour mettre des 0 ou l'element a été ajouté 

	for(var i=0; i<size; i++) {
		for(var j=0; j < size; j++) {
			if(matrix[i+x][j+y] == element){
				matrix[i+x][j+y] = 0;
			}
		}
	}
	
	return matrix; 
}


//Retourne vrai si il reste un carré vide commencant a x, y de taille size dans la tab2d matrix
function IsPlaceAvaible(matrix, size, x, y){
	
	
	if(  (x+ (size -1)) < matrix.length && (y+ (size - 1 )) < matrix[0].length){
			
		//on parcours la sous-matrice voir si tout les elements sont vides
		for(var i=0; i< size; i++) {
			for(var j=0; j < size; j++) {
				if(matrix[i+x][j+y] != 0){
					return false;
				}
			}
		}

		return true;
	
	}else{
		return false;
	}
}

//Retourne vrai si il reste une case vide dans la matrice
function EmptyCase(matrix){
	
	for(var i=0; i<matrix.length; i++) {
		for(var j=0; j<matrix[i].length; j++) {
			if(matrix[i][j] == -1){
				return matrix[i][j];
			}
		}
	}
	
	return false;
}

//Retourne un element de la ville 'r' residential  'b' building 'p' public place
//En fonction de la distribution de probabilité passé en argument 
function randomCityElementWithProbability(buildingProb, residentialProb, publicPlaceProb) {
	
  var notRandomNumbers = [];
  
  for(var i = 0; i < buildingProb; i++){
	  notRandomNumbers.push('b');
  }
  
   for(var i = 0; i < residentialProb; i++){
	  notRandomNumbers.push('r');
  }
  
  for(var i = 0; i < publicPlaceProb; i++){
	  notRandomNumbers.push('p');
  }
  
  var idx = Math.floor(Math.random() * notRandomNumbers.length);
  return notRandomNumbers[idx];
  
}


//Get size element
// Retourne la taille qu'il prendre sur la carte (longueur du carré)
//C'est ici que l'on definit la taille de chaque element (peut etre changer en changeant les return)
function getSizeElement(cityElement) {
	
	if(cityElement == 'b'){
		return 3;
	}else if(cityElement == 'r'){
		return 2;
	}else if(cityElement == 'p'){
		return 4;
	}else if(cityElement == 'h' || cityElement == 'v' || cityElement == 'c' || cityElement == 'd' ){
		return 1;
	}
	
	return 0; 
	
}


//Valeur aléatoire entre min et max 
function randomBetween(min,max){
	return Math.floor((Math.random() * max) + min);
}

//Donne la distance du point x y par rapport au centre de la matrice
function distanceToCenter(x,y,matrix){
	var centerX = matrix[0].length/2; 
	var centerY = matrix.length/2; 
	
	//console.log("X "+centerX+ "Y " + centerY);
	
	var distanceToCenterX = centerX - x;
	var distanceToCenterY = centerY - y;
	
	return Math.sqrt((distanceToCenterX * distanceToCenterX) + (distanceToCenterY * distanceToCenterY));
	
}
