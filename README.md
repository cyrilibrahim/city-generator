# README #
City 3D  procedural generator
Live Exemple: https://s3.amazonaws.com/citygeneratorprocedural/index.html
Source Model: http://www.filedropper.com/ressourcescitygenerator
### Qu'est ce que c'est? ###

* 1.0
* Permet de générer des villes 3d en fonction de la taille de la ville, et de la proportion de chaque élément de la ville 
*(Public, résidentielle, building, etc).

### Comment sa marche? ###
* Les commandes: Utiliser les fleches pour vous deplacer et U et D pour bouger en hauteur
* On ne peut recharger la ville, il faut recharger la page si on veut changer les parametres
* Rayon de la ville est un pourcentage qui indique a partir de quand il y aura plus de tour et la delimitation avec la banlieu
* Les voitures suivents un chemin aleatoire.
