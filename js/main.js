// Variables globales
var scene;
var camera;
var distance = 0;
var controls;
var coli = new Array();
var croisement = new Array();
var voitures = new Array();
var coliTemp = new Array();
var MaxX = 0;
var MinX = 0;
var MaxZ = 0;
var MinZ = 0;
var positionCubeX;
var positionCubeZ;
//Par rapport a blender et three 1 unite de notre ville correspond a 12.0 (1 unite = 1 carrée de route = 1 carrée divers) 
var unite = 12;
var clock = new THREE.Clock();
var dayDuration = 50;
var rays = [
new THREE.Vector3(0, 0, 1),
new THREE.Vector3(1, 0, 1),
new THREE.Vector3(1, 0, 0),
new THREE.Vector3(1, 0, -1),
new THREE.Vector3(0, 0, -1),
new THREE.Vector3(-1, 0, -1),
new THREE.Vector3(-1, 0, 0),
new THREE.Vector3(-1, 0, 1)

];

var dossier = "Modele/";

//GENERATION DE LA MATRICE DE LA VILLE

//Tous les elements suivants de configuration doivent êtres présent pour que la génération marche
var CONFIG = {};

//Grandeur de la ville 
CONFIG.HEIGHT_CITY = 100;
CONFIG.WIDTH_CITY = 100;


//Facteur de présences des elements de la ville 
CONFIG.PUBLIC_PLACE_PRESENCE = 10;
CONFIG.RESIDENTIAL_PRESENCE = 10;
CONFIG.BUILDING_PRESENCE = 20;

//Pourcentage de banlieu centre-ville en pourcentage
CONFIG.CENTREVILLE_RAYON = 55;


//Création de notre plan de ville dans un tableau 2d
var cityMatrix = createCityMatrix(CONFIG);
showMatrix(cityMatrix);

/*On liste ici tous les elements 3d, pour ajouter un modele mettre le .json dans le dossier Modele puis l'ajouter a la liste correspondante 
Chaque liste correspondont a l'element de la matrice généré voici un rappel des elements:
  
	- 'v' = rue vertical	
	- 'h' = rue horizontal		
	- 'c' = croisement de rue	
	- 'p' = espace public	  	
	- 'r' = place residentiel  
	- 'b' = building			
	- 'd' = divers(ex: arbre)	
	*/

	var cityObjects = new Array();

	cityObjects.rBanlieu = [
	{ name: dossier + "maison1.json" }, { name: dossier + "maison2.json" }, { name: dossier + "maison3.json" },
	{ name: dossier + "maison1T2.json" }, { name: dossier + "maison2T2.json" }, { name: dossier + "maison3T2.json" },
	{ name: dossier + "maison1T3.json" }, { name: dossier + "maison2T3.json" }, { name: dossier + "maison3T3.json" },
	{ name: dossier + "maison1T4.json" }, { name: dossier + "maison2T4.json" }, { name: dossier + "maison3T4.json" },
	{ name: dossier + "maison1T5.json" }, { name: dossier + "maison2T5.json" }, { name: dossier + "maison3T5.json" }];

	cityObjects.rVille = [
	{ name: dossier + "maisonVille1T1.json" }];

//Buildings
cityObjects.bVille 		 = [{name: dossier + "highRise1T1.json"},{name: dossier + "highRise2T1.json"},{name: dossier + "highRise3T1.json"}];
cityObjects.bBanlieu 	 = [{name: dossier + "highRise1T1.json"}];

//Divers
cityObjects.dVille 		 = [{name: dossier + "divers1.json"},{name: dossier + "divers2.json"},{name: dossier + "divers2T2.json"},{name: dossier + "divers3.json"}];
cityObjects.dBanlieu	 = [{name: dossier + "divers1.json"},{name: dossier + "divers2.json"},{name: dossier + "divers2T2.json"},{name: dossier + "divers3.json"}];

//Place public 
cityObjects.pVille 		 = [{name: dossier + "placePublic1.json"}];
cityObjects.pBanlieu	 = [{name: dossier + "placePublic1.json"}];

cityObjects.road = [{ name: dossier + "road.json" }, { name: dossier + "roadCroisement.json" }];

cityObjects.cars = [{ name: dossier + "taxi.json" }];

var elementsCharger = 0; 

function start(){
	CONFIG.HEIGHT_CITY = document.getElementById("height").value;
	CONFIG.WIDTH_CITY = document.getElementById("width").value;
	CONFIG.PUBLIC_PLACE_PRESENCE =  document.getElementById("place").value;
	CONFIG.BUILDING_PRESENCE =  document.getElementById("bureau").value;
	CONFIG.RESIDENTIAL_PRESENCE =  document.getElementById("res").value;
	CONFIG.CENTREVILLE_RAYON = document.getElementById("rayon").value;
	initializeScene(); 
	animateScene();
}
// Initialisation de la scène
//initializeScene(); 


// Animation de la scène
//animateScene();






function initializeScene() { 
    // Initialisation du canvas
    renderer = new THREE.WebGLRenderer({ antialias: true });

    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;
    renderer.setSize(canvasWidth, canvasHeight);
    renderer.setClearColor(0xA1DFff, 1);
    document.getElementById("canvas").appendChild(renderer.domElement); 


    // Initialisation de la scène et de la caméra
    scene = new THREE.Scene();

	//Add Fog
	scene.fog = new THREE.Fog(0xffffff, 0.015, 400);

	var light = new THREE.HemisphereLight(0xffffbb, 0x080820, 1);
	scene.add(light);

	var light2 = new THREE.AmbientLight(0x404040); // soft white light
	scene.add(light2);
	

    // Chargement du plancher
    loadFloor();

	//Charge tous les modeles 3d
	loadObjects();

	var nombreElementsACharger = getNumberAllElements(cityObjects);
	
	//Loader qui ajoute les objet à la fin du chargement de tous les modeles 
	loaderTimeOut(nombreElementsACharger);

}

function loaderTimeOut(nombreElementsACharger) {
	setTimeout(function () {

		if (elementsCharger < nombreElementsACharger) {
			console.log("Progression :" + elementsCharger + "/" + nombreElementsACharger);
			loaderTimeOut(nombreElementsACharger);
		} else {
			console.log("Fini :" + elementsCharger + "/" + nombreElementsACharger);
			addObjects();

			cube.position.x = croisement[0].x;
			cube.position.z = croisement[0].z;
		};
	}
	, 300);
}


// Chargement du plancher
function loadFloor() {
	var floorGeometry = new THREE.Geometry();
	floorGeometry.vertices.push(new THREE.Vector3(-(CONFIG.HEIGHT_CITY / 2) * unite, 0.0, (CONFIG.WIDTH_CITY / 2) * unite));
	floorGeometry.vertices.push(new THREE.Vector3((CONFIG.HEIGHT_CITY / 2) * unite, 0.0, (CONFIG.WIDTH_CITY / 2) * unite));
	floorGeometry.vertices.push(new THREE.Vector3((CONFIG.HEIGHT_CITY / 2) * unite, 0.0, -(CONFIG.WIDTH_CITY / 2) * unite));
	floorGeometry.vertices.push(new THREE.Vector3(-(CONFIG.HEIGHT_CITY / 2) * unite, 0.0, -(CONFIG.WIDTH_CITY / 2) * unite));
	floorGeometry.faces.push(new THREE.Face3(0, 1, 2));
	floorGeometry.faces.push(new THREE.Face3(0, 2, 3));
	var floorMaterial = new THREE.MeshBasicMaterial({
		color: 0x202020,
		side: THREE.DoubleSide,
	});
	var floorMesh = new THREE.Mesh(floorGeometry, floorMaterial);
	floorMesh.position.set(0, 0, 0);

	scene.add(floorMesh);

}


function saveObjectHelper(path) {
	return function (geometry, materials) {
		var meshMaterial = new THREE.MeshFaceMaterial(materials);
		for (var i = 0; i < materials.length; ++i) {
			materials[i].side = THREE.DoubleSide;
		}

		var object = new THREE.Mesh(geometry, meshMaterial);

		path.object = object;



		incrementElementCharger();
	}
}

function loadObjects() {
	var loader = new THREE.JSONLoader();

	for (var listeModel in cityObjects) {

		if (cityObjects.hasOwnProperty(listeModel)) {

			for (var j = 0; j < cityObjects[listeModel].length; j++) {
				var path = cityObjects[listeModel][j];
				loader.load(
					cityObjects[listeModel][j].name,
					saveObjectHelper(path));
			}
		}
	}
}


function addObjects() {
	
	//Ajout du personnage 
	cube = new THREE.Mesh(new THREE.CubeGeometry(2, 2, 2), new THREE.MeshNormalMaterial());
	cube.position.set(1, 1, 1);
	scene.add(cube);
	//Ajouter voiture
	
	
	//AJOUT DES ELEMENTS DE LA MAP POUR CREER LA VILLE

	camera = new THREE.PerspectiveCamera(75, canvasWidth / canvasHeight, 0.1, 400);
	camera.position.set(cube.position.x, cube.position.y-10, cube.position.z - 20)
	camera.lookAt(cube.position);
	scene.add(camera);	
	//Point en haut a gauche 
	var DebutMap = new THREE.Vector3(-(CONFIG.HEIGHT_CITY / 2) * unite, 0.0, -(CONFIG.WIDTH_CITY / 2) * unite);

	for (var i = 0; i < CONFIG.HEIGHT_CITY; i++) {
		for (var j = 0; j < CONFIG.WIDTH_CITY; j++) {

			var element = cityMatrix[i][j];
			var size = getSizeElement(element);

			var object = 0; 
			
			//Distance du point au centre
			var distanceCenter = distanceToCenter(i, j, cityMatrix);
			
			//Ajout des voitures 
			if (element == "h") {
				
				var rand = Math.random();
				if (rand < 0.20) {
					var position = new THREE.Vector3(0, 0, 0);
					position.addVectors(DebutMap, new THREE.Vector3((i + (size / 2)) * unite, 0.0, (j + (size / 2)) * unite));
					var voiture = cityObjects.cars[0].object.clone();
					voiture.position.set(position.x-1.1,0.5, position.z);
					voiture.sens = 1;
					voiture.rotation.y = (Math.PI / 2);
					voitures.push(voiture);	
					coli.push(voiture);
					scene.add(voiture);
				}else if(rand<0.40){
					var position = new THREE.Vector3(0, 0, 0);
					position.addVectors(DebutMap, new THREE.Vector3((i + (size / 2)) * unite, 0.0, (j + (size / 2)) * unite));
					var voiture = cityObjects.cars[0].object.clone();
					voiture.rotation.y = ( 3* Math.PI / 2);
					voiture.position.set(position.x+1.1,0.5, position.z);
					voiture.sens = 0;
					voitures.push(voiture);
					coli.push(voiture);
					scene.add(voiture);
				}
				
				
			}
			//Ajout des elements immobiles
			switch (element) {
				case 'h':

				object = cityObjects.road[0].object.clone();
				object.rotation.y = (Math.PI / 2);

				break;
				case 'v':
				object = cityObjects.road[0].object.clone();

				break;
				case 'c':

				object = cityObjects.road[1].object.clone();
				position.x > MaxX ? MaxX = position.x : position.x = position.x;
				position.x < MinX ? MinX = position.x : position.x = position.x;
				position.z > MaxZ ? MaxZ = position.z : position.z = position.z;
				position.z < MinZ ? MinZ = position.z : position.z = position.z;
				positionCubeX = position.Z;
				positionCubeZ = position.Z;
				croisement.push(object.position);
				break;
				default:

				if (cityObjects.hasOwnProperty(element + "Ville") || cityObjects.hasOwnProperty(element + "Banlieu")) {


						//Decide si on choisi la liste de banlieu ou bien centre ville 
						if (distanceCenter > CONFIG.CENTREVILLE_RAYON) {
							//Get random element in the list of element 
							object = cityObjects[element + "Banlieu"][Math.floor(Math.random() * cityObjects[element + "Banlieu"].length)].object.clone();
						} else {
							//Get random element in the list of element 
							object = cityObjects[element + "Ville"][Math.floor(Math.random() * cityObjects[element + "Ville"].length)].object.clone();
						}
						
						
						//Fait une rotation face a la route 
						faceToRoad(cityMatrix, i, j, size, object);
						//Ajoute l'element dans la liste des colisions	
						coli.push(object);
						cityMatrix = addElementToMap(cityMatrix, size, i, j, element);

					}
					break;
				}


				if (object != 0) {
					var position = new THREE.Vector3(0, 0, 0);
					position.addVectors(DebutMap, new THREE.Vector3((i + (size / 2)) * unite, 0.0, (j + (size / 2)) * unite));

					object.position.x = position.x;
					object.position.y = position.y + 0.2;
					object.position.z = position.z;

					scene.add(object);
				}
			}
		}
	}

//Face to road
//Retourne un objet vers la route 
function faceToRoad(matrix, i, j, size, object) {
	if (j + (size) <= matrix.length) {
		if (matrix[i][j + (size)] == 'h' || matrix[i][j + (size)] == 'v') {
			return 0;
		}
	}
	if (j > 0) {

		if (matrix[i][j - 1] == 'h' || matrix[i][j - 1] == 'v') {

			object.rotation.y = (Math.PI);
			return 0;
		}

	}
	if (i > 0) {
		if (matrix[i - 1][j] == 'h' || matrix[i - 1][j] == 'v') {
			object.rotation.y = -(Math.PI / 2);
			return 0;
		}

	}
	if ((i + size) < matrix[i].length) {
		if (matrix[i + (size)][j] == 'h' || matrix[i + (size)][j] == 'v') {
			object.rotation.y = (Math.PI / 2);
			return 0;
		}
	}

}

function animateScene() {
	var timer = new Date().getTime() * 0.0002;

	requestAnimationFrame(animateScene);
	renderScene();
	setTimeout(function(){
		var camNewPos = rotate(cube.position.x, cube.position.z, cube.position.x, cube.position.z + 10, cube.rotation.y);
	camera.position.set(camNewPos[0], cube.position.y + 3, camNewPos[1]);
	camera.lookAt(cube.position);
},2000)
	
}

function renderScene() {
	updatePositionVehicule();
	renderer.render(scene, camera);

	//controls.update( clock.getDelta() );
} 
//update vehicule Position
function updatePositionVehicule() {
	voitures.forEach(function (voiture) {
		var croisement = checkPositionVoiture(voiture);
		if(croisement){
			var diff =  new Date() - voiture.lastVirage;
			if(diff > 500 || voiture.lastVirage == undefined ){
				voiture.lastVirage = new Date();
				var rand = Math.random();
				if(rand < 0.25 || croisement.x >= MaxX || croisement.x <= MinX || croisement.z >= MaxZ || croisement.z <= MinZ || voiture.HaveToTurn ){
					voiture.rotation.y += (3 *Math.PI / 2);
					voiture.HaveToTurn = false;
				}
			}
		}
		if(voiture.position.x < MinX - 40 || voiture.position.x > MaxX  +	 40 || voiture.position.z < MinZ  - 40 || voiture.position.z> MaxZ  + 40) 
		{
			voiture.rotation.y += (Math.PI);
			voiture.HaveToTurn = true;
		}
		voiture.translateX(-0.15);
	});
}

function checkPositionVoiture(v){
	var res = false;
	croisement.forEach(function(c){
		if( Math.abs(c.z - v.position.z) < 1 && Math.abs(c.x - v.position.x) < 2 ){
			res = c;
			return;
		}
	})
	return res;
}

//Aide au controller de camera
function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize(window.innerWidth, window.innerHeight);

	controls.handleResize();
}

//Donne le nombre de modele 3d à charger 
function getNumberAllElements(cityElements) {
	var nombre = 0;

	for (var property in cityElements) {
		if (cityElements.hasOwnProperty(property)) {
			nombre = cityElements[property].length + nombre;
		}
	}

	return nombre;
}

setInterval(function () {
	coliTemp = coli.filter(function (item) {
		if(item.position == undefined){
			console.log(item);}
			var x2 = item.position.x;
			var x1 = cube.position.x;
			var y2 = item.position.z;
			var y1 = cube.position.z;
			var temp = Math.sqrt((x2 -= x1) * x2 + (y2 -= y1) * y2);
			return temp < 1000;
		});
}, 50)

function incrementElementCharger() {
	elementsCharger++;
}


