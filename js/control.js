document.onkeydown = function (e) {


	switch (e.keyCode) {

        case 37: // Left arrow
        cube.rotation.y += Math.PI/32;
            //camera.rotation.y += 0.2;
            break;
        case 39: // Right arrow
        cube.rotation.y -= Math.PI/32;
        break;
        case 38: // Up arrow
        cube.translateZ(-1);
        break;
        case 40: // Down arrow
        cube.translateZ(1);
        break;
        case 85: // U key
        cube.position.y += 0.5;
        break;
        case 68: // D key
        cube.position.y -= 0.5;
        break;
    }
    

    var caster = new THREE.Raycaster();
    var min = 999999;
    var res ;
    for(var j = 0;j<coliTemp.length;j++){
    	
    	var x2 = coliTemp[j].position.x;
    	var x1 = cube.position.x;
    	var y2= coliTemp[j].position.z;
    	var y1 = cube.position.z;
    	var temp = Math.sqrt( (x2-=x1)*x2 + (y2-=y1)*y2 );
    	if(min > temp){
    		min = temp;
    		res = coliTemp[j];
    	}
    }
    
    var coliTemp2 = [res];
    var collisions;
    var allow = true;
    for ( var i = 0; i < rays.length; i += 1) {
    	
    	caster.set(cube.position, rays[i]);
    	try{
    		collisions = caster.intersectObjects(coliTemp2);
    	}catch(e){
    		
    	}
    	if (collisions) {
    		for (var p = 0; p < collisions.length; p++) {
    			if (collisions[p].distance < 1.5) {
    				allow = false;
    				break;
    			}
    		}
    	};
		// And disable that direction if we do
		
	}
	if (allow == false) {
		switch (e.keyCode) {

			case 37: // Left arrow
			cube.rotation.y -= 0.2;
				//camera.rotation.y += 0.2;
				break;
			case 39: // Right arrow
			cube.rotation.y += 0.2;
			break;
			case 38: // Up arrow
			cube.translateZ(1);
			break;
			case 40: // Down arrow
			cube.translateZ(-1);
			break;
			case 85: // U key
			cube.position.y -= 0.5;
			break;
			case 68: // D key
			cube.position.y += 0.5;
			break;
		}
	}



	var camNewPos = rotate(cube.position.x, cube.position.z, cube.position.x, cube.position.z + 10, cube.rotation.y);
	camera.position.set(camNewPos[0], cube.position.y + 3, camNewPos[1]);
	camera.lookAt(cube.position);
};

function rotate(cx, cy, x, y, radians) {
	var cos = Math.cos(radians),
	sin = Math.sin(radians),
	nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
	ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
	return [nx, ny];
}